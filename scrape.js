const request = require('request');
const cheerio = require('cheerio');
graph="https://merolagani.com/handlers/TechnicalChartHandler.ashx?type=get_advanced_chart&symbol=ADBL&resolution=1D&rangeStartDate=1576909666&rangeEndDate=1611124126&from=&isAdjust=1&currencyCode=NPR"
request('https://www.sharesansar.com/today-share-price',(error, response, html)=>{
    if(!error && response.statusCode==200){
        // console.log(html)
        const $ = cheerio.load(html);
        // const table = $('table');
        // console.log(table.html())
        // console.log(table.text())
        // const output = table.find('td').text();
        // console.log(output);
        var nepse = []
        $('table tbody tr').each((i,el)=>{
            const symbol = $(el).find('a').text().replace(/\s\s+/g,'');
            const title = $(el).find('a').attr('title');
            // const scores = $(el).find('.text-center').text();
            // console.log(symbol,' ',title,' ',scores);
            var varData = [];
            $(el).find('.text-center').each((index,data)=>{
                // console.log($(data).text())
                varData.push(Number($(data).text().replace(/,/g,'')));
            })
            // console.log(symbol,' ',title,' ',varData);
            nepse.push({
                'symbol': symbol,
                'companyName': title,
                'conf': varData[0],
                'open':varData[1],
                'high':varData[2],
                'low':varData[3],
                'close':varData[4],
                'VWAP':varData[5],
                'vol':varData[6],
                'prevClose':varData[7],
                'turnover':varData[8],
                'trans':varData[9],
                'diff':varData[10],
                '120days':varData[15],
                '180days':varData[16],
                '52WeeksHigh':varData[17],
                '52WeeksLow':varData[18]
            })
        })
        console.log(nepse)
    }
});