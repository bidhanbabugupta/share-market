var router = require('express').Router();
const request = require('request');
const cheerio = require('cheerio');
var multer = require('multer');
const csvtojson = require('csvtojson');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});
function imageFilter(req, file, cb) {
    // console.log(file.mimetype)
    var imageType = file.mimetype.split('/')[1];
    if (imageType == 'csv') {
        cb(null, true);
    } else {
        req.fileError = true;
        cb(null, false);
    }
}
var upload = multer({
    storage: storage,
    fileFilter: imageFilter
});

router.post('/upload',upload.single('img'),(req,res,next)=>{

   csvtojson()
    .fromFile('./files/images/'+req.file.filename)
    .then((json)=>{
        let data =[]
    let comp = []
    json.forEach((val)=>{
        comp.push(val.Scrip)
    })
    comp=[...new Set(comp)]
    comp.forEach((val)=>{
        let script ={
            symbol:'',
            total:'',
            share:[]
        }
        let i=0;
        for (i=0;i<json.length-1;i++){
           if(json[i].Scrip==val){
               script.symbol = val;
                script.total=json[i]['Balance After Transaction'];
    
               json.forEach((cval)=>{
                
                    if(cval.Scrip == val){
                        script.share.push([cval['History Description'].split(" ")[0],cval['Credit Quantity'],cval['Debit Quantity'],cval['Transaction Date']]);
                    }
                })
                data.push(script) 
                break;
           }   
        }
      
    })
    res.json(data)
    })
    .catch((err)=>next(err))
})


router.get('/ipo-approved',(req,res,next)=>{
    request('http://sebon.gov.np/ipo-approved',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/ipo-pipeline',(req,res,next)=>{
    request('http://sebon.gov.np/ipo-pipeline',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/right-share-approved',(req,res,next)=>{
    request('http://sebon.gov.np/right-share-approved',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/right-share-pipeline',(req,res,next)=>{
    request('http://sebon.gov.np/right-share-pipeline',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/debenture-approved',(req,res,next)=>{
    request('http://sebon.gov.np/debenture-approved',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/debenture-pipeline',(req,res,next)=>{
    request('http://sebon.gov.np/debenture-pipeline',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        var ipoApproved =[];
         $('table tbody tr').each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text())
                }
                if($(data).find('a').attr('href')){
                    ipo.push($(data).find('a').attr('href'))
                }
            })
            ipoApproved.push(ipo)
        })
        res.json(ipoApproved)
    }else{
        next({msg:"cannot fetch data"})
    }
    })
})

router.get('/today-share-price',(req,res,next)=>{
    request('https://www.sharesansar.com/today-share-price',(error, response, html)=>{
    if(!error && response.statusCode==200){
        // console.log(html)
        const $ = cheerio.load(html);
        // const table = $('table');
        // console.log(table.html())
        // console.log(table.text())
        // const output = table.find('td').text();
        // console.log(output);
        var nepse = []
        $('table tbody tr').each((i,el)=>{
            const symbol = $(el).find('a').text().replace(/\s\s+/g,'');
            const title = $(el).find('a').attr('title');
            var varData = [];
            $(el).find('.text-center').each((index,data)=>{
                varData.push(Number($(data).text().replace(/,/g,'')));
            })
            nepse.push({
                'symbol': symbol,
                'companyName': title,
                'conf': varData[0],
                'open':varData[1],
                'high':varData[2],
                'low':varData[3],
                'close':varData[4],
                'VWAP':varData[5],
                'vol':varData[6],
                'prevClose':varData[7],
                'turnover':varData[8],
                'trans':varData[9],
                'diff':varData[10],
                '120days':varData[15],
                '180days':varData[16],
                '52WeeksHigh':varData[17],
                '52WeeksLow':varData[18]
            })
        })
        res.json(nepse)
    }else{
        next({msg:'cannot fetch data'})
    }
    })
})

router.get('/top',(req,res,next)=>{
    var top={gainers:[],losers:[],transaction:[]};
request('http://www.nepalstock.com/',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        // console.log(html)
        const $ = cheerio.load(html);
        const table = $("#top-losers  tbody");
        var transaction=[]
        $("#top-by-transaction  tbody tr").each((i,el)=>{
            var topgain=[]
            console.log($(el).find("td").text())
            $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    topgain.push($(data).text());
                }
            })
            transaction.push(topgain);
        })
      
        top.transaction = transaction;

        var gainers=[]
        $("#top-gainers  tbody tr").each((i,el)=>{
            var topgain=[]
            console.log($(el).find("td").text())
            $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    topgain.push($(data).text());
                }
            })
            gainers.push(topgain);
        })
        
        top.gainers = gainers;
    
    var losers=[]
        $("#top-losers  tbody tr").each((i,el)=>{
            var topgain=[]
            console.log($(el).find("td").text())
            $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    topgain.push($(data).text());
                }
            })
            losers.push(topgain);
        })
        top.losers = losers;
        res.json(top)
    }else{
        next({msg:"connot fetch top data"})
    }
    });
})
router.get('/indices',(req,res,next)=>{
    request('http://www.nepalstock.com/',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        // console.log(html)
        const $ = cheerio.load(html);
        const table = $("#option2  tbody");
        // console.log(table.html())
        var transaction=[]
        $("#nepse-stats .panel-body tbody tr").each((i,el)=>{
            var topgain=[]
            // console.log($(el).find("td").text())
            $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    topgain.push($(data).text().replace("\n","").trim());
                }
            })
            transaction.push(topgain);
        })
        res.json(transaction)
    }else{
        next({msg:"Cannot fetch data"})
    }
});
})
router.get('/gold',(req,res,next)=>{
    request('http://www.fenegosida.org/',async (error, response, html)=>{
        if(!error && response.statusCode==200){
            // console.log(html)
            const $ = cheerio.load(html);
            const table = $("#header-rate");
            // console.log(table.text())
            var transaction={gold:[],silver:[]}
            $("#header-rate .rate-gold").each((i,el)=>{
                transaction.gold.push($(el).text());
            })
            $("#header-rate .rate-silver").each((i,el)=>{
                transaction.silver.push($(el).text());
            })
            res.json(transaction)
        }else{
            next({msg:"cannot fetch data"})
        }
    });
})
router.get('/transaction',(req,res,next)=>{
    request('https://www.nrb.org.np/',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        // console.log(html)
        const $ = cheerio.load(html);
        const table = $(".card-bt .card-bt-body table");
        console.log(table.text())
        var transaction=[]
        $(".card-bt .card-bt-body table tbody tr").each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text().replace(/\s\s+/g,'').trim())
                }
            })
            transaction.push(ipo)
        })
        res.json(transaction)
    }else{
        next({msg:"could not send data"})
    }
});
})
router.get('/floorsheet',(req,res,next)=>{
    request('https://merolagani.com/Floorsheet.aspx',async (error, response, html)=>{
    if(!error && response.statusCode==200){
        const $ = cheerio.load(html);
        const table = $("table");
        var transaction=[]
        $(" table tbody tr").each((i,el)=>{
            var ipo=[];
             $(el).find('td').each((index,data)=>{
                if($(data).text()){
                    ipo.push($(data).text().replace(/\s\s+/g,'').trim())
                }
            })
            transaction.push(ipo)
        })
        res.json(transaction)
    }else{
        next({msg:"Cannot receive floorsheet"})
    }
})
})
router.get('/marketdepth',(req,res,next)=>{
    request('http://www.nepalstock.com/marketdepth',async (error, response, html)=>{
        if(!error && response.statusCode==200){
            const $ = cheerio.load(html);
            var transaction=[]
            $("#StockSymbol_Select option").each((i,el)=>{
                var ipo={symbol:'',val:''};
                 ipo.symbol=$(el).text();
                 ipo.val=$(el).attr('value');
                transaction.push(ipo)
            })
            res.json(transaction)
        }else{
            next({msg:"could not get marketdepth"})
        }
    })
})
router.get('/marketdepth/:id',(req,res,next)=>{
    var id = req.params.id;
    request(`http://www.nepalstock.com/marketdepthofcompany/${id}`,async (error, response, html)=>{
        if(!error && response.statusCode==200){
            const $ = cheerio.load(html);
            var transaction=[]
            $("table .orderTable ").each((i,el)=>{
               
                var ipo=[];
                 $(el).find('tbody tr').each((index,data)=>{
                     var abt=[];
                    $(data).find('td').each((index,data)=>{ 
                        if($(data).text()){
                            abt.push($(data).text().replace(/\s\s+/g,'').trim())
                        }
                    })
                    ipo.push(abt)
                })
                transaction.push(ipo)
                
            })
            res.json(transaction)
        }else{
            next({msg:"No  such data available"})
        }
    })
})
router.get('/brokers',(req,res,next)=>{
    request('http://www.nepalstock.com/brokers',async (error, response, html)=>{
        if(!error && response.statusCode==200){
            const $ = cheerio.load(html);
            // console.log(html)
            const table = $('.content').html()
            // console.log(table)
            var broker=[]
            $('.content').each((i,el)=>{
                var bro=[]
                // console.log($(el).find('h4').text())
                bro.push($(el).find('h4').text())
                $(el).find('.row-content').each((i,el)=>{
                    bro.push($(el).text().replace(/\s\s+/g,'').trim())
                })
                broker.push(bro)
            })
           res.json(broker)
        }else{
            next({msg:"could not get marketdepth"})
        }
    })
})
router.get('/symbol/:id',(req,res,next)=>{
    var id = req.params.id;
    request(`https://merolagani.com/CompanyDetail.aspx?symbol=${id}`,async (error, response, html)=>{
        if(!error && response.statusCode==200){
            const $ = cheerio.load(html);
        // console.log(html)
        // const table = $('.row .panel-body table').html()
        // console.log(table)
        var out=[]
        $('.row .panel-body table tbody').each((i,el)=>{
            var data={
                title:'',
                value:''
            }
            data.title = $(el).find('th').text().replace(/\s\s+/g,'').trim();
            data.value = $(el).find('td').text().replace(/\s\s+/g,'').trim();
            out.push(data)
        })
        res.json(out)
        }else{
            next({msg:"could not get marketdepth"})
        }
    })
})
router.get('/graph/:num/:id',(req,res,next)=>{
    var num = req.params.num;
    var id = req.params.id;
    request(`http://www.nepalstock.com/graphdata/${num}/${id}`, {json: true}, (error, response, body) => {
        if (error) {
            return  console.log(error)
        };

        if (!error && res.statusCode == 200) {
            // do something with JSON, using the 'body' variable
           res.json([body])
        }else{
            next({msg:'not working'})
        };
    });
})
module.exports = router;